import * as fs from "fs";
import * as path from "path";
import { CommentItem, parse, PingItem, PostItem } from "@/peg/posts";

const file = path.resolve(__dirname, "../samples/post-with-long-bar.mtif");
const input = fs.readFileSync(file).toString();

describe("peg parser: post with long bar", () => {
  let subject: PostItem[];

  beforeEach(() => {
    subject = parse(input)[0];
  });

  it("throws no exception", () => {
    expect(() => parse(input)).not.toThrowError();
  });

  it("returns 16 elements", () => {
    expect(subject).toHaveLength(16);
  });
  it("returns AUTHOR element", () => {
    expect(subject[0]).toHaveProperty("title", "AUTHOR");
    expect(subject[0]).toHaveProperty("value", "Sample Author");
  });
  it("returns TITLE element", () => {
    expect(subject[1]).toHaveProperty("title", "TITLE");
    expect(subject[1]).toHaveProperty("value", "Sample Title");
  });
  it("returns BASENAME element", () => {
    expect(subject[2]).toHaveProperty("title", "BASENAME");
    expect(subject[2]).toHaveProperty("value", "sample-filename");
  });
  it("returns STATUS element", () => {
    expect(subject[3]).toHaveProperty("title", "STATUS");
    expect(subject[3]).toHaveProperty("value", "Publish");
  });
  it("returns ALLOW COMMENTS element", () => {
    expect(subject[4]).toHaveProperty("title", "ALLOW COMMENTS");
    expect(subject[4]).toHaveProperty("value", "1");
  });
  it("returns ALLOW PINGS element", () => {
    expect(subject[5]).toHaveProperty("title", "ALLOW PINGS");
    expect(subject[5]).toHaveProperty("value", "0");
  });
  it("returns CONVERT BREAKS element", () => {
    expect(subject[6]).toHaveProperty("title", "CONVERT BREAKS");
    expect(subject[6]).toHaveProperty("value", "richtext");
  });
  it("returns PRIMARY CATEGORY element", () => {
    expect(subject[7]).toHaveProperty("title", "PRIMARY CATEGORY");
    expect(subject[7]).toHaveProperty("value", "Sample");
  });
  it("returns CATEGORY element", () => {
    expect(subject[8]).toHaveProperty("title", "CATEGORY");
    expect(subject[8]).toHaveProperty("value", "Sample");
  });
  it("returns CATEGORY element", () => {
    expect(subject[9]).toHaveProperty("title", "CATEGORY");
    expect(subject[9]).toHaveProperty("value", "Test");
  });
  it("returns DATE element", () => {
    expect(subject[10]).toHaveProperty("title", "DATE");
    expect(subject[10]).toHaveProperty("value", "08/11/2018 08:00:00 PM");
  });
  it("returns TAGS element", () => {
    expect(subject[11]).toHaveProperty("title", "TAGS");
    expect(subject[11]).toHaveProperty("value", '"Sample post",test,spec');
  });
  it("returns IMAGE element", () => {
    expect(subject[12]).toHaveProperty("title", "IMAGE");
    expect(subject[12]).toHaveProperty("value", "https://img.example.com/image.jpg");
  });
  it("returns BODY element", () => {
    expect(subject[13]).toHaveProperty("title", "BODY");
    expect(subject[13]).toHaveProperty("value", [
      "<h1>sample</h1>",
      "",
      "<p>sample text!</p>"
    ]);
  });

  describe("COMMENT element", () => {
    let contents: CommentItem[];
    let value: string[];

    beforeEach(() => {
      const comment = subject[14];
      if (comment.title == "COMMENT") {
        contents = comment.contents;
        value = comment.value;
      }
    });

    it("is COMMENT element", () => {
      expect(subject[14]).toHaveProperty("title", "COMMENT");
    });

    it("returns 2 contents", () => {
      expect(contents).toHaveLength(2);
    });

    it("returns AUTHOR content", () => {
      expect(contents[0]).toHaveProperty("title", "AUTHOR");
      expect(contents[0]).toHaveProperty("value", "Sample Comment Author");
    });

    it("returns DATE content", () => {
      expect(contents[1]).toHaveProperty("title", "DATE");
      expect(contents[1]).toHaveProperty("value", "01/31/2020 15:40:00");
    });

    it("returns value", () => {
      expect(value).toEqual(["sample comment text!", "", "comment!"]);
    });
  });

  describe("PING element", () => {
    let contents: PingItem[];
    let value: string[];

    beforeEach(() => {
      const ping = subject[15];
      if (ping.title == "PING") {
        contents = ping.contents;
        value = ping.value;
      }
    });

    it("is PING element", () => {
      expect(subject[15]).toHaveProperty("title", "PING");
    });

    it("returns 3 contents", () => {
      expect(contents).toHaveLength(3);
    });

    it("returns TITLE content", () => {
      expect(contents[0]).toHaveProperty("title", "TITLE");
      expect(contents[0]).toHaveProperty("value", "Sample ping title");
    });

    it("returns URL content", () => {
      expect(contents[1]).toHaveProperty("title", "URL");
      expect(contents[1]).toHaveProperty(
        "value",
        '<a href="http://www.example.com">http://www.example.com/baz/archives/000015.html</a>'
      );
    });

    it("returns DATE content", () => {
      expect(contents[2]).toHaveProperty("title", "DATE");
      expect(contents[2]).toHaveProperty("value", "15/07/2020 04:15:00 PM");
    });

    it("returns value", () => {
      expect(value).toEqual(["sample ping!", "", "ping!"]);
    });
  });
});
