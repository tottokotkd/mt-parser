import * as fs from "fs"
import * as path from "path"
import { parse, PostItem } from "@/peg/posts";

const file = path.resolve(__dirname, '../samples/posts.mtif')
const input = fs.readFileSync(file).toString()

describe("peg parser: multiple posts", () => {
  it("throws no exception", () => {
    expect(() => parse(input)).not.toThrowError()
  })

  it("returns array", () => {
    expect(parse(input)).toBeInstanceOf(Array);
  });
  describe("item 1", () => {
    let subject: PostItem[];
    beforeEach(() => {
      subject = parse(input)[0];
    });

    it ("returns 13 elements", () =>{
      expect(subject).toHaveLength(13)
    })

    it("returns AUTHOR element", () => {
      expect(subject[0]).toHaveProperty("title", "AUTHOR");
      expect(subject[0]).toHaveProperty("value", "Sample Author 1");
    });
    it("returns TITLE element", () => {
      expect(subject[1]).toHaveProperty("title", "TITLE");
      expect(subject[1]).toHaveProperty("value", "Sample Title 1");
    });
    it("returns BASENAME element", () => {
      expect(subject[2]).toHaveProperty("title", "BASENAME");
      expect(subject[2]).toHaveProperty("value", "sample-filename-1");
    });
    it("returns STATUS element", () => {
      expect(subject[3]).toHaveProperty("title", "STATUS");
      expect(subject[3]).toHaveProperty("value", "Publish");
    });
    it("returns ALLOW COMMENTS element", () => {
      expect(subject[4]).toHaveProperty("title", "ALLOW COMMENTS");
      expect(subject[4]).toHaveProperty("value", "1");
    });
    it("returns ALLOW PINGS element", () => {
      expect(subject[5]).toHaveProperty("title", "ALLOW PINGS");
      expect(subject[5]).toHaveProperty("value", "0");
    });
    it("returns CONVERT BREAKS element", () => {
      expect(subject[6]).toHaveProperty("title", "CONVERT BREAKS");
      expect(subject[6]).toHaveProperty("value", "richtext");
    });
    it("returns PRIMARY CATEGORY element", () => {
      expect(subject[7]).toHaveProperty("title", "PRIMARY CATEGORY");
      expect(subject[7]).toHaveProperty("value", "Sample1");
    });
    it("returns CATEGORY element", () => {
      expect(subject[8]).toHaveProperty("title", "CATEGORY");
      expect(subject[8]).toHaveProperty("value", "Sample1");
    });
    it("returns CATEGORY element", () => {
      expect(subject[9]).toHaveProperty("title", "CATEGORY");
      expect(subject[9]).toHaveProperty("value", "Test1");
    });
    it("returns DATE element", () => {
      expect(subject[10]).toHaveProperty("title", "DATE");
      expect(subject[10]).toHaveProperty("value", "08/11/2018 08:00:00 PM");
    });
    it("returns TAGS element", () => {
      expect(subject[11]).toHaveProperty("title", "TAGS");
      expect(subject[11]).toHaveProperty("value", '"Sample post 1",test,spec');
    });
    it("returns BODY element", () => {
      expect(subject[12]).toHaveProperty("title", "BODY");
      expect(subject[12]).toHaveProperty("value", [
        "<h1>sample 1</h1>",
        "",
        "<p>sample text!</p>"
      ]);
    });
  });
  describe("item 2", () => {
    let subject: PostItem[];
    beforeEach(() => {
      subject = parse(input)[1];
    });

    it ("returns 13 elements", () =>{
      expect(subject).toHaveLength(13)
    })

    it("returns AUTHOR element", () => {
      expect(subject[0]).toHaveProperty("title", "AUTHOR");
      expect(subject[0]).toHaveProperty("value", "Sample Author 2");
    });
    it("returns TITLE element", () => {
      expect(subject[1]).toHaveProperty("title", "TITLE");
      expect(subject[1]).toHaveProperty("value", "Sample Title 2");
    });
    it("returns BASENAME element", () => {
      expect(subject[2]).toHaveProperty("title", "BASENAME");
      expect(subject[2]).toHaveProperty("value", "sample-filename-2");
    });
    it("returns STATUS element", () => {
      expect(subject[3]).toHaveProperty("title", "STATUS");
      expect(subject[3]).toHaveProperty("value", "Publish");
    });
    it("returns ALLOW COMMENTS element", () => {
      expect(subject[4]).toHaveProperty("title", "ALLOW COMMENTS");
      expect(subject[4]).toHaveProperty("value", "0");
    });
    it("returns ALLOW PINGS element", () => {
      expect(subject[5]).toHaveProperty("title", "ALLOW PINGS");
      expect(subject[5]).toHaveProperty("value", "1");
    });
    it("returns CONVERT BREAKS element", () => {
      expect(subject[6]).toHaveProperty("title", "CONVERT BREAKS");
      expect(subject[6]).toHaveProperty("value", "richtext");
    });
    it("returns PRIMARY CATEGORY element", () => {
      expect(subject[7]).toHaveProperty("title", "PRIMARY CATEGORY");
      expect(subject[7]).toHaveProperty("value", "Sample2");
    });
    it("returns CATEGORY element", () => {
      expect(subject[8]).toHaveProperty("title", "CATEGORY");
      expect(subject[8]).toHaveProperty("value", "Sample2");
    });
    it("returns CATEGORY element", () => {
      expect(subject[9]).toHaveProperty("title", "CATEGORY");
      expect(subject[9]).toHaveProperty("value", "Test2");
    });
    it("returns DATE element", () => {
      expect(subject[10]).toHaveProperty("title", "DATE");
      expect(subject[10]).toHaveProperty("value", "08/11/2018 08:00:00 PM");
    });
    it("returns TAGS element", () => {
      expect(subject[11]).toHaveProperty("title", "TAGS");
      expect(subject[11]).toHaveProperty("value", '"Sample post 2",test,spec');
    });
    it("returns BODY element", () => {
      expect(subject[12]).toHaveProperty("title", "BODY");
      expect(subject[12]).toHaveProperty("value", [
        "<h1>sample 2</h1>",
        "",
        "<p>sample text!</p>"
      ]);
    });
  });
});
