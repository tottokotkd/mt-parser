import * as fs from "fs"
import * as path from "path"
import { parse, PostItem } from "@/peg/posts";

const file = path.resolve(__dirname, '../samples/post-with-body.mtif')
const input = fs.readFileSync(file).toString()

describe("peg parser: post with body", () => {
  let subject: PostItem[];
  beforeEach(() => {
    subject = parse(input)[0];
  });

  it("throws no exception", () => {
    expect(() => parse(input)).not.toThrowError();
  });

  it("returns 13 elements", () => {
    expect(subject).toHaveLength(13);
  });
  it("returns AUTHOR element", () => {
    expect(subject[0]).toHaveProperty("title", "AUTHOR");
    expect(subject[0]).toHaveProperty("value", "Sample Author");
  });
  it("returns TITLE element", () => {
    expect(subject[1]).toHaveProperty("title", "TITLE");
    expect(subject[1]).toHaveProperty("value", "Sample Title");
  });
  it("returns BASENAME element", () => {
    expect(subject[2]).toHaveProperty("title", "BASENAME");
    expect(subject[2]).toHaveProperty("value", "sample-filename");
  });
  it("returns STATUS element", () => {
    expect(subject[3]).toHaveProperty("title", "STATUS");
    expect(subject[3]).toHaveProperty("value", "Publish");
  });
  it("returns ALLOW COMMENTS element", () => {
    expect(subject[4]).toHaveProperty("title", "ALLOW COMMENTS");
    expect(subject[4]).toHaveProperty("value", "1");
  });
  it("returns ALLOW PINGS element", () => {
    expect(subject[5]).toHaveProperty("title", "ALLOW PINGS");
    expect(subject[5]).toHaveProperty("value", "0");
  });
  it("returns CONVERT BREAKS element", () => {
    expect(subject[6]).toHaveProperty("title", "CONVERT BREAKS");
    expect(subject[6]).toHaveProperty("value", "richtext");
  });
  it("returns PRIMARY CATEGORY element", () => {
    expect(subject[7]).toHaveProperty("title", "PRIMARY CATEGORY");
    expect(subject[7]).toHaveProperty("value", "Sample");
  });
  it("returns CATEGORY element", () => {
    expect(subject[8]).toHaveProperty("title", "CATEGORY");
    expect(subject[8]).toHaveProperty("value", "Sample");
  });
  it("returns CATEGORY element", () => {
    expect(subject[9]).toHaveProperty("title", "CATEGORY");
    expect(subject[9]).toHaveProperty("value", "Test");
  });
  it("returns DATE element", () => {
    expect(subject[10]).toHaveProperty("title", "DATE");
    expect(subject[10]).toHaveProperty("value", "08/11/2018 08:00:00 PM");
  });
  it("returns TAGS element", () => {
    expect(subject[11]).toHaveProperty("title", "TAGS");
    expect(subject[11]).toHaveProperty("value", '"Sample post",test,spec');
  });
  it("returns BODY element", () => {
    expect(subject[12]).toHaveProperty("title", "BODY");
    expect(subject[12]).toHaveProperty("value", [
      "<h1>sample</h1>",
      "",
      "-----aaa" ,
      "----- aaa" ,
      "----- aaa ",
      "space after short bar ->",
      "-----",
      "",
      "<p>sample text!</p>"
    ]);
  });
});
