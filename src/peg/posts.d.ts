export declare const parse: (input: string) => Post[];

type Post = PostItem[];

type PostItem =
  | {
      title: SingleLineElement;
      value: string;
    }
  | {
      title: MultiLineElement;
      value: string[];
    }
  | {
      title: "COMMENT";
      contents: { title: CommentElement; value: string }[];
      value: string[];
    }
  | {
      title: "PING";
      contents: { title: PingElement; value: string }[];
      value: string[];
    };

type SingleLineElement =
  | "AUTHOR"
  | "TITLE"
  | "BASENAME"
  | "STATUS"
  | "ALLOW COMMENTS"
  | "ALLOW PINGS"
  | "CONVERT BREAKS"
  | "PRIMARY CATEGORY"
  | "CATEGORY"
  | "CATEGORY"
  | "DATE"
  | "TAGS"
  | "NO ENTRY";

type MultiLineElement = "BODY" | "EXTENDED BODY" | "EXCERPT" | "KEYWORDS";

type CommentElement = "AUTHOR" | "EMAIL" | "URL" | "IP" | "DATE";
type CommentItem = {
  title: CommentElement;
  value: string;
};

type PingElement = "TITLE" | "URL" | "IP" | "BLOG NAME" | "DATE";
type PingItem = {
  title: PingElement;
  value: string;
};
